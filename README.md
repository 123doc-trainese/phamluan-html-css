# Bài tập Html Css

***HTML & CSS***
```
- Tìm hiểu html, css cơ bản
- Sử dụng scss
- Sử dụng bootstrap và tailwind
```

## Liên kết

- [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
- [CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- [Boostrap](https://getbootstrap.com/docs/5.2/getting-started/introduction/)
- [Tailwind](https://tailwindcss.com/docs/installation)
- [w3schools](https://www.w3schools.com/html/default.asp)

## Kiến thức nắm được

***HTML***
```
- HTML (Hypertext Markup Language) - ngôn ngữ đánh dấu siêu văn bản
- HTML mô tả cấu trúc của một trang web, bao gồm một loạt các phần tử
- Các phần tử HTML cho trình duyệt biết cách hiển thị nội dung
- Khai báo <!DOCTYPE html> để xác định đây là một tài liệu html, chỉ khai báo một lần ở đầu trang
- Cấu trúc một phần tử html: <tagname> Nội dung </tagname>
- Ví dụ: <h1> Đây là tiêu đề h1 </h1>

```
***CSS***

- Css (Cascading Style Sheets) được sử dụng để xác định kiểu cho các trang web của bạn, bao gồm thiết kế, bố cục và các biến thể hiển thị cho các thiết bị và kích thước màn hình khác nhau
- Sử dụng css trong html:
	1. External CSS
	2. Internal CSS
	3. Inline CSS
- Độ ưu tiên css: !important -> inline -> id -> class -> tag. Nếu độ ưu tiên bằng nhau thì trình duyệt sẽ  lấy thuộc tính được khai báo cuối cùng
- Cấu trúc: selector{thuộc tính 1: giá trị 1; thuộc tính 2: giá trị 2;}


***Bootstrap***

- Bootstrap là một front-end framework miễn phí để phát triển web nhanh hơn và dễ dàng hơn 
- Cung cấp khả năng tạo các trang web tự động điều chỉnh để có giao diện đẹp trên tất cả các thiết bị
- Bootstrap bao gồm các mẫu thiết kế dựa trên HTML và CSS
- Khai báo sử dụng: có thể sử dụng CDN hoặc tải về bootstrap
- Ví dụ sử dụng CDN: <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">

***Tailwind css***

- Tailwind css là một css framework, cũng giống như bootstrap là có những class xây dựng sẵn ma chúng ta có thể sử dụng được
- Các class có thể sử dụng lại thông qua từ khóa @apply, ví dụ
	.demo {
    	     @apply flex justify-between items-center py-4 bg-blue-900;
	}
- Có thể viết sự kiện cho các class như hover, forcus, disable,...
- [Hướng dẫn cài đặt tailwind](https://tailwindcss.com/docs/installation)
- Dùng CDN: `<script src="https://cdn.tailwindcss.com"></script>`

***Scss***
- SASS/SCSS là một chương trình tiền xử lý CSS, giúp viết CSS theo cách của một ngôn ngữ lập trình, có cấu trúc rõ ràng, rành mạch, dễ phát triển và bảo trì code hơn
- Khai báo biến dễ dàng hơn và có thể được sử dụng lại trong một file .scss khác
- Phân cấp các selector lồng nhau trực quan, rõ ràng
- [Tổng quan về scss](https://sass-lang.com/guide)
- [Hướng dẫn cài đặt](https://www.youtube.com/watch?v=77QwgOBOg7Y)
- Sử dụng lệnh: `npm install -g sass` để cài đặt sass
- Tạo file `style.scss`
- Sử dụng lệnh: `sass style.scss style.css --watch` để biên dịch từ scss sang css
